from setuptools import setup

with open("readme.md", "r") as fh:
    long_description = fh.read()

with open("VERSION", "r", encoding='utf-8') as version_file:
    version = version_file.read().strip()

setup(
    name='dumper',
    version=version,
    packages=['dumper'],
    url='https://gitlab.uriit.ru/packages/python/dumper',
    license='GPLv3+',
    author='Izert Mansur',
    author_email='izertmi@uriit.ru',
    description='Dumper – tool for creating dumps',
    install_requires=[
        'typer==0.3.2',
        'pydantic>=1.8.1',
        'sshtunnel>=0.4.0'
    ],
    keywords=['Database', 'PostgreSQL', 'Dump'],
    long_description=long_description,
)
