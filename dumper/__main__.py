from pathlib import Path
from typing import Optional

import typer

from .environ import load_environ
from .exceptions import ConfigError, ExecuteException
from .commands import dump as dump_command, drain as drain_command, restore_database as restore_command, restore_database

console_app = typer.Typer()


@console_app.command('dump')
def dump(environ: str = typer.Argument(..., help='Name of the environ defined in the dumper.ini file'),
         dump_location: Optional[str] = typer.Option(None, help='Custom location for dump'),
         with_name: bool = typer.Option(False, help='if location was specified with the filename'),
         dump_format: str = typer.Option('plain', help='Dump format (not supported for now)'),
         config_path: str = typer.Option('./dumper.ini', help='Custom location of dumper.ini config')):
    """
    Dump database in chosen environment
    """
    config_path = Path(config_path)
    if not config_path.exists():
        typer.echo(f'Unable to locate config by path {config_path}', err=True)
        raise typer.Exit(1)

    if dump_format != 'plain':
        typer.echo(f'Dump format "{dump_format}" is not supported', err=True)
        raise typer.Exit(2)

    try:
        environ_config, global_config = load_environ(environ, config_path.absolute())
    except ConfigError as e:
        typer.echo(f'Unable to load environ "{environ}"', err=True)
        typer.echo(f'Fail reason: "{e}"', err=True)
        raise typer.Exit(3)

    try:
        dump_command(global_config, environ, config_path.absolute(), environ_config)
    except ExecuteException as e:
        typer.echo(f'Failed to execute dump command. Reason: ', err=True)
        raise typer.Exit(e.code)


@console_app.command('drain')
def drain(from_environ: str = typer.Argument(..., help='Name of the environ defined in the dumper.ini file'),
          to_environ: str = typer.Argument(..., help='Name of the environ defined in the dumper.ini file'),
          dump_location: Optional[str] = typer.Option(None, help='Custom location for dump'),
          with_name: bool = typer.Option(False, help='if location was specified with the filename'),
          dump_format: str = typer.Option('plain', help='Dump format (not supported for now)'),
          config_path: str = typer.Option('./dumper.ini', help='Custom location of dumper.ini config')):
    """
    Dump database in chosen environment
    """
    config_path = Path(config_path)
    if not config_path.exists():
        typer.echo(f'Unable to locate config by path {config_path}', err=True)
        raise typer.Exit(1)

    if dump_format != 'plain':
        typer.echo(f'Dump format "{dump_format}" is not supported', err=True)
        raise typer.Exit(2)

    try:
        from_environ_config, global_config = load_environ(from_environ, config_path.absolute())
        to_environ_config, _ = load_environ(to_environ, config_path.absolute())
    except ConfigError as e:
        typer.echo(f'Unable to load environ "{from_environ}"', err=True)
        typer.echo(f'Unable to load environ "{to_environ}"', err=True)
        typer.echo(f'Fail reason: "{e}"', err=True)
        raise typer.Exit(3)

    try:
        drain_command(global_config, from_environ, to_environ, config_path, from_environ_config, to_environ_config)

    except ExecuteException as e:
        typer.echo(f'Failed to execute dump command. Reason: ', err=True)
        raise typer.Exit(e.code)


@console_app.command('restore')
def dump(environ: str = typer.Argument(..., help='Name of the environ defined in the dumper.ini file'),
         file: str = typer.Argument(..., help='Dump location'),
         dump_format: str = typer.Option('plain', help='Dump format (not supported for now)'),
         config_path: str = typer.Option('./dumper.ini', help='Custom location of dumper.ini config')):
    """
    Dump database in chosen environment
    """
    config_path = Path(config_path)
    if not config_path.exists():
        typer.echo(f'Unable to locate config by path {config_path}', err=True)
        raise typer.Exit(1)

    if dump_format != 'plain':
        typer.echo(f'Dump format "{dump_format}" is not supported', err=True)
        raise typer.Exit(2)

    try:
        environ_config, global_config = load_environ(environ, config_path.absolute())
    except ConfigError as e:
        typer.echo(f'Unable to load environ "{environ}"', err=True)
        typer.echo(f'Fail reason: "{e}"', err=True)
        raise typer.Exit(3)

    try:
        if not Path(file).exists():
            typer.echo(f'Unable to locate dump file {config_path}', err=True)
            raise typer.Exit(1)
        restore_database(global_config, environ_config, file)
    except ExecuteException as e:
        typer.echo(f'Failed to execute dump command. Reason: ', err=True)
        raise typer.Exit(e.code)


if __name__ == '__main__':
    console_app()
