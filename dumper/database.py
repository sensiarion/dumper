from abc import abstractmethod, ABC
from typing import List, Tuple


class DatabaseConfiguration(ABC):
    def __init__(self, db_name: str, db_user: str, db_password: str, db_port: int, db_host: str, **compile_options):
        self.db_host = db_host
        self.db_port = db_port
        self.db_password = db_password
        self.db_user = db_user
        self.db_name = db_name
        self.compile_options = compile_options

    @abstractmethod
    def dump_utils_names(self) -> List[str]:
        pass

    @abstractmethod
    def restore_utils_names(self) -> List[str]:
        pass

    @abstractmethod
    def dump_compile(self, filename: str, util_params: str) -> str:
        pass

    @abstractmethod
    def restore_compile(self, dump_name: str, util_params: str) -> str:
        pass

    @abstractmethod
    def reserved_params(self) -> Tuple[str, ...]:
        pass

    def supply_password(self) -> str:
        return self.db_password


class PostgresConfiguration(DatabaseConfiguration):
    def dump_utils_names(self) -> List[str]:
        return ['pg_dump']

    def restore_utils_names(self) -> List[str]:
        return ['psql', 'pg_dump']

    def dump_compile(self, filename: str, util_params: str) -> str:
        return f'pg_dump -w {util_params} --username "{self.db_user}" --host {self.db_host} --port {self.db_port} --dbname "{self.db_name}" -f {filename}'

    def restore_compile(self, dump_name: str, util_params: str) -> str:
        return f'pg_restore -c -w {util_params} --username "{self.db_user}" --host {self.db_host} --port {self.db_port} --dbname "{self.db_name}" {dump_name}'

    def execute_command(self, command: str):
        return f'psql -w --username "{self.db_user}" --host {self.db_host} --port {self.db_port} --dbname "{self.db_name}"  -c "{command}"'

    def reserved_params(self) -> Tuple[str, ...]:
        pass

    def drop_db(self):
        return f'dropdb --if-exists --username "{self.db_user}" --host {self.db_host} --port {self.db_port} "{self.db_name}"'
