import os
from pathlib import Path

from dumper.exceptions import ConfigError


def is_installed(util_name: str, console: str, encoding='utf-8') -> bool:
    return True


def find_versioned_filename(semi_generated_template: str):
    version = 1
    while os.path.exists(semi_generated_template.format(version=str(version))):
        version += 1

    return semi_generated_template.format(version=str(version))


class SafeDict(dict):
    def __missing__(self, key):
        return '{' + key + '}'


def generate_filename(directory: str, template: str, template_params: dict, config_location: Path) -> str:
    # TODO: refactor directories
    if '{version}' not in template:
        raise ConfigError("Template should have a place for a {version}")

    try:
        template_params.pop('version')
    except KeyError:
        pass

    # TODO refactor on search in specified shell console
    if directory:
        directory_path = Path(directory)
        if not directory_path.exists():
            raise ConfigError(f'Directory "{directory}" not found')
    else:
        directory_path = config_location.parent

    if not directory_path.is_dir():
        raise ConfigError(f'Directory "{directory}" actually is not a directory')

    # no version specified
    semi_compiled_template = template.format_map(SafeDict(**template_params))
    semi_compiled_full_path = directory_path.joinpath(semi_compiled_template).absolute()
    versioned_name = find_versioned_filename(str(semi_compiled_full_path))
    return versioned_name
