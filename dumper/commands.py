import datetime
from pathlib import Path
import os

import typer

from dumper.database import PostgresConfiguration, DatabaseConfiguration
from dumper.environ import run_console_command, ssh_tunnel
from dumper.exceptions import ConfigError, ExecuteException
from dumper.settings_format import GlobalConfig, EnvironConfig
from dumper.utils import is_installed, generate_filename


def dump(global_conf: GlobalConfig, environ_name: str, config_location: Path,
         environ_conf: EnvironConfig, backend_configuration: DatabaseConfiguration = None) -> str:
    command_backer = PostgresConfiguration(db_name=environ_conf.db_name,
                                           db_user=environ_conf.db_user,
                                           db_password=environ_conf.db_password,
                                           db_host=environ_conf.db_host,
                                           db_port=environ_conf.db_port)

    for required_util in command_backer.dump_utils_names():
        if not is_installed(required_util, global_conf.console, encoding=global_conf.encoding):
            raise ConfigError(f"You need to install {required_util} in your console shell, before use")

    today = datetime.date.today()
    file_path = generate_filename(global_conf.dump_location, global_conf.name_template, {
        'year': str(today.year),
        'month': str(today.month),
        'day': str(today.day),
        'environ_name': environ_name.lower()
    }, config_location)

    command = command_backer.dump_compile(file_path, global_conf.backend_settings)

    if environ_conf.ssh_url:
        with ssh_tunnel(environ_conf, environ_conf.db_port) as tunnel:
            result = run_console_command(command, global_conf.console, global_conf.encoding,
                                         dict(PGPASSWORD=environ_conf.db_password))
    else:
        result = run_console_command(command, global_conf.console, global_conf.encoding,
                                     dict(PGPASSWORD=environ_conf.db_password))
    return file_path


def restore_database(global_conf: GlobalConfig, environ_conf: EnvironConfig, dump_path: str):
    backend_configuration = PostgresConfiguration(db_name=environ_conf.db_name,
                                                  db_user=environ_conf.db_user,
                                                  db_password=environ_conf.db_password,
                                                  db_host=environ_conf.db_host,
                                                  db_port=environ_conf.db_port)

    def exec():
        restore_command = backend_configuration.restore_compile(dump_path, '')
        run_console_command(restore_command, global_conf.console, global_conf.encoding,
                            dict(PGPASSWORD=environ_conf.db_password))

    if environ_conf.ssh_url:
        with ssh_tunnel(environ_conf, environ_conf.db_port) as tunnel:
            exec()
    else:
        exec()


def drain(global_conf: GlobalConfig, from_environ_name: str, to_environ_name: str, config_location: Path,
          from_environ_conf: EnvironConfig, to_environ_conf: EnvironConfig,
          backend_configuration: DatabaseConfiguration = None):
    migrating_dump = dump(global_conf, from_environ_name, config_location, from_environ_conf, backend_configuration)
    if not os.path.exists(migrating_dump):
        typer.echo('Migrating dump not created')
        return

    save_dump = dump(global_conf, to_environ_name, config_location, to_environ_conf, backend_configuration)
    if not os.path.exists(save_dump):
        typer.echo('Save dump before drain not created')
        return

    restore_database(global_conf, to_environ_conf, migrating_dump)
