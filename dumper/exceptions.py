from typing import Optional


class DumperException(Exception):
    """
    Core exception
    """


class ConfigError(DumperException):
    """
    Configuration error
    """


class ExecuteException(DumperException):
    """
    Some trouble to run dump commands
    """

    def __init__(self, code: int, err_verbose: Optional[str], err_info: Optional[str]):
        self.err_info = err_info
        self.err_verbose = err_verbose
        self.code = code
