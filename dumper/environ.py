import configparser
import os
import subprocess
import sys
from contextlib import contextmanager
from pathlib import Path
from typing import Tuple, Dict

import pydantic
import sshtunnel
from pydantic import ConfigError

from dumper.exceptions import ExecuteException
from dumper.settings_format import EnvironSSHConfig, EnvironConfig, GlobalConfig


@contextmanager
def ssh_tunnel(config: EnvironSSHConfig, bind_to: int) -> sshtunnel.SSHTunnelForwarder:
    user, host = config.ssh_url.split('@')
    sshtunnel.SSH_TIMEOUT = 5.0
    sshtunnel.TUNNEL_TIMEOUT = 5.0

    with sshtunnel.SSHTunnelForwarder(
            (host, 22),
            ssh_password=config.ssh_password,
            ssh_username=user,
            remote_bind_address=('127.0.0.1', config.ssh_remote_db_port),
            local_bind_address=('127.0.0.1', bind_to)
    ) as tunnel:
        yield tunnel


def load_environ(env_name: str, env_location: Path) -> Tuple[EnvironConfig, GlobalConfig]:
    parser = configparser.ConfigParser()
    parser.read(env_location)

    global_section = dict(parser['GLOBAL'])
    try:
        global_config = GlobalConfig(**global_section) if global_section else GlobalConfig()
    except pydantic.ValidationError as e:
        raise ConfigError("Unable to parse config file: " + str(e))

    environ_section = dict(parser[env_name])
    try:
        environ_config = EnvironConfig(**environ_section)
    except pydantic.ValidationError as e:
        raise ConfigError("Unable to parse config file: " + str(e))

    return environ_config, global_config


@contextmanager
def environment_variables(**vars):
    for key, value in vars.items():
        os.environ[key] = value
    yield
    for key, value in vars.items():
        os.environ.pop(key)


def run_console_command(command: str, console: str, encoding='utf-8', environ_vars: dict = None) -> str:
    if '{command}' not in console:
        raise ConfigError('Console template is incorrect. It should have {command} block to execute properly')
    compiled_command = console.format(command=f'"{command}"')
    print(compiled_command)

    environ_vars = {} if environ_vars is None else environ_vars

    with environment_variables(**environ_vars):
        code = os.system(compiled_command)
    if code != 0:
        raise ExecuteException(code, None, None)

    return ''
