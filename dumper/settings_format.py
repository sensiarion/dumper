from typing import Optional

import pydantic


class GlobalConfig(pydantic.BaseModel):
    dump_before: bool = True
    name_template = "dump_{environ_name}_{year}.{month}.{day}_{version}.dmp"
    dump_location: pydantic.DirectoryPath = None
    console = "wsl {command}"
    encoding = 'utf-8'
    backend_settings: Optional[str]


class EnvironSSHConfig(pydantic.BaseModel):
    ssh_url: Optional[str]
    ssh_password: Optional[str]
    ssh_port: int = 22
    ssh_remote_db_port: Optional[int]

    @pydantic.root_validator()
    def ensure_remote_port_configured_if_ssh_enabled(cls, data):
        if data.get('ssh_url') and not data.get('ssh_remote_db_port'):
            raise ValueError("Ensure remote db port is provided")

        return data

    @pydantic.root_validator(skip_on_failure=True)
    def ensure_have_ssh_password(cls, data: dict):
        if data.get('SSH_URL') and data.get('SSH_PASSWORD') is None:
            raise ValueError("No ssh password supplied")

        return data


class EnvironConfig(EnvironSSHConfig):
    db_name: str
    db_user: str
    db_password: str
    db_host: str
    db_port: int
